<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "listing_photos".
 *
 * @property int $sysid
 * @property string $PictureCount
 * @property string|null $URLs
 * @property string|null $Resource
 * @property string|null $Class
 * @property string|null $Photo_Download
 * @property string $PhotoModificationTimestamp
 * @property int $id
 * @property string $list_MLSnum
 * @property string $rc_ModifiedTime
 * @property int|null $order_id
 */
class Listing_photos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'listing_photos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sysid', 'PictureCount', 'PhotoModificationTimestamp', 'list_MLSnum'], 'required'],
            [['sysid', 'order_id'], 'integer'],
            [['URLs'], 'string'],
            [['rc_ModifiedTime'], 'safe'],
            [['PictureCount', 'Resource', 'Class', 'Photo_Download'], 'string', 'max' => 100],
            [['PhotoModificationTimestamp'], 'string', 'max' => 64],
            [['list_MLSnum'], 'string', 'max' => 16],
            [['sysid'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sysid' => 'Sysid',
            'PictureCount' => 'Picture Count',
            'URLs' => 'Ur Ls',
            'Resource' => 'Resource',
            'Class' => 'Class',
            'Photo_Download' => 'Photo Download',
            'PhotoModificationTimestamp' => 'Photo Modification Timestamp',
            'id' => 'ID',
            'list_MLSnum' => 'List Ml Snum',
            'rc_ModifiedTime' => 'Rc Modified Time',
            'order_id' => 'Order ID',
        ];
    }
}
