<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "property_type".
 *
 * @property string $PropertyTypeCode
 * @property string $PropertyTypeName
 * @property int $order
 */
class Property_type extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'property_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PropertyTypeCode', 'PropertyTypeName', 'order'], 'required'],
            [['order'], 'integer'],
            [['PropertyTypeCode'], 'string', 'max' => 25],
            [['PropertyTypeName'], 'string', 'max' => 50],
            [['PropertyTypeCode'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PropertyTypeCode' => 'Property Type Code',
            'PropertyTypeName' => 'Property Type Name',
            'order' => 'Order',
        ];
    }
}
